'use strict';

angular.module('jgangularPlaygroundApp')

.directive('jgcbox', function () {
	return {
		restrict: 'E',
		scope: {
			jginput: '@',
			jgoutput: '='
		},
		replace: true,
		template: '<div class="row">' +
				  '<div class="col-md-6"><input type="checkbox" ng-model="jgcboxbool"/><label>{{jginput}}</label></div>' +
				  '<div class="col-md-6" ng-show="jgcboxbool">{{jgoutput.ja}}</div>' +
				  '<div class="col-md-6" ng-hide="jgcboxbool">{{jgoutput.nej}}</div>' +
				  '</div>'
	}
})